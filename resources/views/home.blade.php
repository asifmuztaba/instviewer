@extends('master')
@section('maincontent')
    <section class="instabox">
        <div class="container">
            <div class="row">

                <?php foreach($data as $d){?>
                <div class="col-md-4">
                    <div class="boxitem">
                        <div class="card" style="">
                            <div class="card-header">
                                <div class="proimage">
                                    <img src="{{$d->user->profile_picture}}" alt="">
                                    <span>{{$d->user->username}}</span>
                                </div>
                            </div>
                            <img class="card-img-top midImage" src="{{$d->images->standard_resolution->url}}" alt="Card image cap">
                            <div class="card-body">
                                <div class="icons">
                                <span class="heart">
                                    <i class="fas fa-heart"></i>{{$d->likes->count}}
                                </span>
                                    <span class="comments">
                                    <i class="fas fa-comments"></i>{{$d->comments->count}}
                                </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php }?>
            </div>
        </div>
    </section>
@endsection