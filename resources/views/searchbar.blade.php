<section class="searchblock">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="searchblockInner">
                    <h1>Instagram Online Viwer</h1>
                    <h4>This is an Instagram Online Viewer that you can easily browse users, location, stories, followers, hashtags, popular contents, statistics and much more.</h4>
                    <div class="searchdiv">
                        <div class="searchcenter">
                            <input type="text" id="homeSearchText" required="required" class="searchcenterinput1" placeholder="Search #tag or @user">
                        </div>
                        <div class="searchright">
                            <a id="searchButton" href="#"
                            ><i class="fa fa-search"></i> SEARCH</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>