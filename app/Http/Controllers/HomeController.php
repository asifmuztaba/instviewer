<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Vinkla\Instagram\Instagram;

class HomeController extends Controller
{
    public function index(){


        $instagram = new Instagram('wednesday','8538736059.1677ed0.cca35f89e02944dc8f6024b4fcec2608');
//        echo '<pre>';
//        var_dump($instagram->get());
//        echo '</pre>';
//        exit();
        $data=$instagram->get();
        return view('home')->with('data',$data);
    }
    public function processURL($url)
    {
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => 2
        ));

        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
    public function getresult(){
        $tag = '99points';
        $client_id = "1699450322.8517740.ed05141b13314cd5a2b24d169853b0b3";
        $url = 'https://api.instagram.com/v1/tags/'.$tag.'/media/recent?client_id='.$client_id;

        $all_result  = $this->processURL($url);
        $decoded_results = json_decode($all_result, true);
        // echo '<pre>';
        // print_r($decoded_results);
        // exit;

        //Now parse through the $results array to display your results...
        foreach($decoded_results['data'] as $item){
            $image_link = $item['images']['thumbnail']['url'];
            echo '<img src="'.$image_link.'" />';
        }
    }


}
